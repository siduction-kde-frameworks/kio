kio (5.12.0-1) unstable; urgency=medium

  * New upstream release (5.12.0).

 -- Maximiliano Curia <maxy@debian.org>  Thu, 09 Jul 2015 12:44:03 +0200

kio (5.11.0-2) unstable; urgency=medium

  * Refresh patch.
  * New patch: fix_freebsd_build.
  * Update symbols files.

 -- Maximiliano Curia <maxy@debian.org>  Tue, 07 Jul 2015 12:02:58 +0200

kio (5.11.0-1) unstable; urgency=medium

  * New upstream release (5.10.0).
  * Update symbols files.
  * New upstream release (5.11.0).

 -- Maximiliano Curia <maxy@debian.org>  Mon, 29 Jun 2015 09:42:03 +0200

kio (5.9.0-1) experimental; urgency=medium

  * New upstream release (5.9.0).

 -- Maximiliano Curia <maxy@debian.org>  Thu, 23 Apr 2015 08:26:19 +0200

kio (5.9.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Mon, 13 Apr 2015 22:28:24 +0200

kio (5.8.0-1) experimental; urgency=medium

  * New upstream release (5.8.0).
  * Update symbols files.
  * New patch: wait_for_a_bit_longer
  * Update copyright information.

 -- Maximiliano Curia <maxy@debian.org>  Sun, 22 Mar 2015 11:38:41 +0100

kio (5.8.0-0ubuntu2) vivid; urgency=medium

  * Add kubuntu_kdelibs4-docs-path.diff to search in kdelibs4 path

 -- Jonathan Riddell <jriddell@ubuntu.com>  Thu, 02 Apr 2015 17:19:09 +0200

kio (5.8.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Tue, 17 Mar 2015 15:38:15 +0100

kio (5.7.0-1) experimental; urgency=medium

  * New upstream release (5.7.0).
  * New patch: return_on_ACCESS_DENIED
  * New patch: report_error_removing_dirs

 -- Maximiliano Curia <maxy@debian.org>  Fri, 06 Mar 2015 22:57:43 +0100

kio (5.7.0-0ubuntu2) vivid; urgency=medium

  * Make sure autopkgtest pulls in own binary packages as Depends
    such that data asset lookup (e.g. protocol configs) can be
    found more reliably

 -- Harald Sitter <sitter@kde.org>  Fri, 13 Feb 2015 14:51:00 +0100

kio (5.7.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Harald Sitter <sitter@kde.org>  Tue, 10 Feb 2015 16:52:44 +0100

kio (5.6.0-1) experimental; urgency=medium

  * Prepare initial Debian release.
  * Add acc autopkgtests.
  * Update build dependencies to build against experimental and to
    follow cmake.
  * Bump Standards-Version to 3.9.6, no changes needed.
  * Update install files.
  * Update watch file.
  * Update symbols files.

 -- Maximiliano Curia <maxy@debian.org>  Thu, 29 Jan 2015 15:12:37 +0100

kio (5.6.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Tue, 06 Jan 2015 20:13:15 +0100

kio (5.5.0-0ubuntu1) vivid; urgency=medium

  [ Rohan Garg ]
  * Make sure man pages don't get installed twice

  [ Scarlett Clark ]
  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Mon, 15 Dec 2014 12:21:32 +0100

kio (5.4.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Fri, 07 Nov 2014 15:05:22 +0100

kio (5.3.0-0ubuntu1) utopic; urgency=medium

  [ Jonathan Riddell ]
  * New upstream release
  * kio Breaks/replaces old kio-extras for moved kio-trash

  [ Scarlett Clark ]
  * Uncomment files triggered by list-missing to install file.
  * Add new files triggered by list-missing to install file

 -- Jonathan Riddell <jriddell@ubuntu.com>  Tue, 07 Oct 2014 11:14:47 +0100

kio (5.2.0-0ubuntu1) utopic; urgency=medium

  [ Jonathan Riddell ]
  * New upstream release
  * Use pkg-kde-tools version 3 scripts

  [ Scarlett Clark ]
  * Patch symbols - removed 2 MISSING private symbols. 
  * Cleanup copyright + move copyright extra details to comment to silence
    space-in-std-shortname-in-dep5-copyright lintian error. 
  * wrap-and-sort 

 -- Jonathan Riddell <jriddell@ubuntu.com>  Mon, 22 Sep 2014 19:45:56 +0200

kio (5.1.0a-0ubuntu1) utopic; urgency=medium

  [ Rohan Garg ]
  * Make sure we add the XS-Testsuite field to debian/control

  [ José Manuel Santamaría Lema ]
  * Mark a couple of private symbols as optional in
    libkf5kiowidgets5.symbols.
  * Update libraries *.install paths so a soname bump doesn't go
    unnoticed.

  [ Scarlett Clark ]
  * New upstream release
  * Remove upstream_kurlrequester-dialog-accept-handling.diff applied upstream.
  * New upstream tar due to regressions.
  * Patch symbols using batchpatch. 

 -- Scarlett Clark <scarlett@scarlettgatelyclark.com>  Tue, 05 Aug 2014 17:15:17 +0200

kio (5.0.0a-0ubuntu1) utopic; urgency=medium

  * Initial stable upstream release

  [ José Manuel Santamaría Lema ]
  * Mark a couple of private symbols as optional in libkf5kiowidgets5.symbols
    and clean up missing symbols from previous versions.

 -- Scarlett Clark <scarlett@scarlettgatelyclark.com>  Tue, 15 Jul 2014 16:11:35 +0200

